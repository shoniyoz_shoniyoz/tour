import './assets/main.css'
// swiper
import 'swiper/scss'
import 'swiper/scss/navigation'
import 'swiper/scss/pagination'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
